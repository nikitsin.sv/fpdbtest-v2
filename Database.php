<?php

namespace FpDbTest;

use Exception;
use mysqli;

class Database implements DatabaseInterface
{
    private mysqli $mysqli;

    public function __construct(mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    public function buildQuery(string $query, array $args = []): string
    {
        $query = $this->replaceVariable($query, $args);
        $query = $this->processStatements($query);

        return $query;
    }

    public function skip(): string
    {
        return -999;
    }

    private function processStatements(string $query): string
    {
        return preg_replace_callback('/{(.*?)}/', function ($matches) {
            return str_contains($matches[0], $this->skip())
                ? ''
                : str_replace(['{', '}'], '', $matches[0]);
        }, $query);
    }

    private function replaceVariable(string $query, array $args): string
    {
        $replacement = array_shift($args);
        $query = preg_replace_callback( '/\?.?/', function ($matches) use ($replacement) {
            return $this->prepareVariable(value: $replacement, specifier: $matches[0]);
        }, $query, 1);

        if (count($args)) {
            return $this->replaceVariable($query, $args);
        }

        return $query;
    }

    private function prepareVariable(array|bool|float|int|string|null $value, string $specifier = '?'): string
    {
        $endSpace = str_ends_with($specifier, ' ') ? ' ' : '';

        if (trim($specifier) === '?') {
            $specifier = $this->recognizeSpecifier($value);
        }

        return match ($specifier) {
            '?d' => $this->prepareInt($value),
            '?f' => $this->prepareFloat($value),
            '?s' => $this->prepareString($value),
            '?a' => $this->prepareArray($value),
            '?#' => $this->prepareIdentifier($value),
            '?n' => $this->prepareNull($value),
            default => throw new Exception('Unknown specifier: ' . var_export([$value, $specifier], true)),
        } . $endSpace;
    }

    private function recognizeSpecifier(mixed $value): string
    {
        return match (gettype($value)) {
            'array' => '?a',
            'boolean', 'integer' => '?d',
            'double' => '?f',
            'string' => '?s',
            'NULL' => '?n',
            default => throw new Exception('Unknown type: ' . var_export($value, true)),
        };
    }

    private function prepareInt(mixed $value): int|string
    {
        if (is_null($value)) {
            return $this->prepareNull($value);
        }

        return sprintf('%d', $value);
    }

    private function prepareFloat(mixed $value): float|string
    {
        if (is_null($value)) {
            return $this->prepareNull($value);
        }

        return sprintf('%f', $value);
    }

    private function prepareString(mixed $value, $quote = "'"): string
    {
        $value = addslashes($value);

        return sprintf("{$quote}%s{$quote}", $value);
    }

    private function prepareNull(mixed $value): string
    {
        if (!is_null($value)) {
            throw new Exception('Value is not null: ' . var_export($value, true));
        }

        return 'NULL';
    }

    private function prepareArray(mixed $array, $isIdentifier = false): string
    {
        if (!is_array($array)) {
            throw new Exception('Value is not array: '. var_export($array, true));
        }

        $list = [];
        $isList = array_is_list($array);
        foreach ($array as $key => $val) {
            $list[] = $isList
                ? $this->prepareVariable($val, $isIdentifier ? '?#' : '?')
                : $this->prepareVariable($key, '?#') . ' = ' . $this->prepareVariable($val);
        }

        return implode(', ', $list);
    }

    private function prepareIdentifier(mixed $value): string
    {
        return match (gettype($value)) {
            'array' => $this->prepareArray($value, isIdentifier: true),
            'string' => $this->prepareString($value, '`'),
            default => throw new Exception('Invalid identifier format: '
                . var_export(['value' => $value, 'type' => gettype($value)], true)),
        };
    }

}
